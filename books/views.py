from django.shortcuts import render

from books.forms import BookForm
from books.models import Book

# Create your views here.
def show_books(request):

    books = [
        {
            "title": "Moby Dick",
            "publish_date": "1851-10-18",
            "description": "A white whale and the meaning of life"
        },
        {
            "title": "The Hobbit",
            "publish_date": "1937-09-18",
            "description": "Some dragons and gold and a ring...."
        },
    ]

    form = BookForm()
    context = {
        "books": books,
        "form": form,
    }
    return render(request, "home.html", context)
